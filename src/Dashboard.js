'use strict';

var d3 = require('d3');

var Heatmap = require('./charts/Heatmap');
//var SurfaceChartWebGL = require('./charts/SurfaceChartWebGL');
var LineChart = require('./charts/LineChart');

function Dashboard(cfg) {
    this.cfg = cfg || {};
    this.dataX = [];
    this.dataY = [];

    this.pointers = {
        x: [],
        y: [],
        c: [],
        colors: d3.scale.category10().domain(d3.range(10))
    };


    this.colorsAvailability = [];
    for (var i = 0, len = 10; i < len; i++) {
        this.colorsAvailability.push(false);
    }
}

Dashboard.prototype.createDivs = function(parentID, width, height) {
    var parent = d3.select(parentID);
    parent.style('display', 'flex');
    parent.style('flex-wrap', 'wrap');
    var names = {
        heatmap: 'heatmap',
        surfaceChart: 'surfaceChart',
        lineChart1: 'lineChart1',
        lineChart2: 'lineChart2'
    };

    parent.append('div')
        .attr('id', names.heatmap)
        .style('width', width)
        .style('height', height / 2);

    //parent.append('div')
        //.attr('id', names.surfaceChart)
        //.style('width', width)
        //.style('height', height)
        //.style('float', 'right');
        //
        //

    parent.append('div')
        .attr('id', names.lineChart1)
        .style('width', width / 2)
        .style('height', height / 2);

    parent.append('div')
        .attr('id', names.lineChart2)
        .style('width', width / 2)
        .style('height', height / 2);

    return names;
};

Dashboard.prototype.createCharts = function(width, height) {
    var cfg = {
        width: width,
        height: height / 2,
        margin: {left: 90, top: 10, right: 60, bottom: 100}
    };
    var cfg2 = {
        width: width / 2,
        height: height / 2,
        margin: {left: 80, top: 10, right: 10, bottom: 100},
        timeOnX: true,
        parseTime: d3.time.format('%Y-%m-%d').parse,
        timeFormat: d3.time.format('%Y-%m-%d')
    };
    var cfg3 = {
        width: width / 2,
        height: height / 2,
        margin: {left: 80, top: 10, right: 10, bottom: 100},
        timeOnX: true,
        parseTime: d3.time.format('%H:%M').parse,
        timeFormat: d3.time.format('%H:%M')
    };

    var charts = {
        heatmap: new Heatmap(cfg),
        //surfaceChart: new SurfaceChartWebGL(cfg),
        lineChart1: new LineChart(cfg2),
        lineChart2: new LineChart(cfg3)
    };

    return charts;
};

Dashboard.prototype.draw = function(parentID, data) {
    var width = (window.innerWidth * 0.95);
    var height = (window.innerHeight * 0.95);

    var charts = this.createCharts(width, height);
    var divsNames = this.createDivs(parentID, width, height);

    console.log(charts);
    charts.heatmap.draw(divsNames.heatmap, data);
    //charts.surfaceChart.draw(divsNames.surfaceChart, data);
    charts.heatmap.onMouseOver(this.onMouseOver(data, divsNames, charts));
    charts.heatmap.onMouseOut(this.onMouseOut(divsNames, charts));
    charts.heatmap.onClick(this.onClick(data, divsNames, charts));
};

Dashboard.prototype.getColor = function() {
    for (var i = 0, len = this.colorsAvailability.length; i < len; i++) {
        if (this.colorsAvailability[i] === false) {
            this.colorsAvailability[i] = true;
            return i;
        }
    }

    return 0;
};

Dashboard.prototype.onMouseOver = function(data, divsNames, charts) {
    var that = this;

    return function(event) {
        var target = event.target || event.srcElement;
        var dx = target.getAttribute('dx');
        var dy = target.getAttribute('dy');

        var yData = data.filter(function(d) {
            return d.x === dx;
        }).map(function(d) {
            return d.y;
        });
        var zData = data.filter(function(d) {
            return d.x === dx;
        }).map(function(d) {
            return d.z;
        });

        var d1 = {
            y: zData,
            x: yData
        };

        var xData = data.filter(function(d) {
            return d.y === dy;
        }).map(function(d) {
            return d.x;
        });
        zData = data.filter(function(d) {
            return d.y === dy;
        }).map(function(d) {
            return d.z;
        });

        var d2 = {
            y: zData,
            x: xData
        };
        console.log(d1, d2);

        that.dataX.push(d1);
        that.dataY.push(d2);

        charts.lineChart1.draw(divsNames.lineChart1, that.dataX);
        charts.lineChart2.draw(divsNames.lineChart2, that.dataY);

        that.dataX.pop();
        that.dataY.pop();


        var x = parseInt(target.getAttribute('x')) + target.getAttribute('width') / 2;
        var y = parseInt(target.getAttribute('y')) + target.getAttribute('height') / 2;

        that.pointers.x.push(x);
        that.pointers.y.push(y);
        that.pointers.c.push(that.getColor());

        charts.heatmap.drawPointers(that.pointers);

        //console.log(that.pointers.c);
        //console.log(that.colorsAvailability);
        that.pointers.x.pop();
        that.pointers.y.pop();
        var index = that.pointers.c.length - 1;
        that.colorsAvailability[that.pointers.c[index]] = false;
        that.pointers.c.pop();
        //console.log(that.pointers.c);
        //console.log(that.colorsAvailability);
    };
};

Dashboard.prototype.onMouseOut = function(divsNames, charts) {
    var that = this;

    return function() {
        if (that.dataX.length > 0) {
            charts.lineChart1.draw(divsNames.lineChart1, that.dataX);
            charts.lineChart2.draw(divsNames.lineChart2, that.dataY);
        } else {
            charts.lineChart1.clear();
            charts.lineChart2.clear();
        }

        charts.heatmap.drawPointers(that.pointers);
    };
};

Dashboard.prototype.onClick = function(data, divsNames, charts) {
    var that = this;

    return function(event) {
        var target = event.target || event.srcElement;
        var dx = target.getAttribute('dx');
        var dy = target.getAttribute('dy');

        var yData = data.filter(function(d) {
            return d.x === dx;
        }).map(function(d) {
            return d.y;
        });
        var zData = data.filter(function(d) {
            return d.x === dx;
        }).map(function(d) {
            return d.z;
        });

        var d1 = {
            y: zData,
            x: yData
        };

        var xData = data.filter(function(d) {
            return d.y === dy;
        }).map(function(d) {
            return d.x;
        });
        zData = data.filter(function(d) {
            return d.y === dy;
        }).map(function(d) {
            return d.z;
        });

        var d2 = {
            y: zData,
            x: xData
        };

        var x = parseInt(target.getAttribute('x')) + target.getAttribute('width') / 2;
        var y = parseInt(target.getAttribute('y')) + target.getAttribute('height') / 2;

        var exists = false;
        var index = -1;
        for (var i = 0, len = that.pointers.x.length; i < len; i++) {
            if (that.pointers.x[i] === x && that.pointers.y[i] === y) {
                exists = true;
                index = i;
            }
        }

        console.log(exists);
        console.log(index);
        console.log(that.pointers.x);
        console.log(that.pointers.y);

        if (exists === false) {
            that.pointers.x.push(x);
            that.pointers.y.push(y);
            that.pointers.c.push(that.getColor());
            that.dataX.push(d1);
            that.dataY.push(d2);
        } else {
            console.log('slice');
            that.pointers.x.splice(index, 1);
            that.pointers.y.splice(index, 1);
            that.colorsAvailability[that.pointers.c[index]] = false;
            that.pointers.c.splice(index, 1);
            that.dataX.splice(index, 1);
            that.dataY.splice(index, 1);
        }

        if (that.dataX.length > 0) {
            charts.lineChart1.draw(divsNames.lineChart1, that.dataX);
            charts.lineChart2.draw(divsNames.lineChart2, that.dataY);
        } else {
            charts.lineChart1.clear();
            charts.lineChart2.clear();
        }
        charts.heatmap.drawPointers(that.pointers);

        console.log(that.pointers.x);
        console.log(that.pointers.y);
        console.log(that.pointers.c);
    };
};

module.exports = Dashboard;
