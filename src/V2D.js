var d3 = require('d3');

var LineChart = require('./charts/LineChart');
var Heatmap = require('./charts/Heatmap');
var SurfaceChartWebGL = require('./charts/SurfaceChartWebGL');
var Dashboard = require('./Dashboard');

var V2D = {
    LineChart: LineChart,
    Heatmap: Heatmap,
    SurfaceChartWebGL: SurfaceChartWebGL,
    Dashboard: Dashboard,
    d3: d3
};

module.exports = V2D;
