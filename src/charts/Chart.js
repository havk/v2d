'use strict';

var d3 = require('d3');

function Chart(cfg) {
    this.cfg = cfg || {};

    this.cfg.width = this.cfg.width || 400;
    this.cfg.height = this.cfg.height || 300;
    this.cfg.margin = this.cfg.margin || {top: 50, right: 50, bottom: 50, left: 50};
    this.cfg.w = this.cfg.width - this.cfg.margin.left - this.cfg.margin.right;
    this.cfg.h = this.cfg.height - this.cfg.margin.top - this.cfg.margin.bottom;
}

Chart.prototype.createSVG = function(parentID) {
    var svg = d3.select('#' + parentID)
        .append('svg')
            .attr('width', this.cfg.width)
            .attr('height', this.cfg.height)
        .append('g')
            .attr('transform', 'translate(' + this.cfg.margin.left + ',' + this.cfg.margin.top + ')');

    return svg;
};

Chart.prototype.beautify = function(svg) {
    svg.selectAll('g.axis').selectAll('path, line')
        .style({
            'fill': 'none',
            'stroke': 'black',
            'shape-rendering': 'crispEdges'
        });
};

module.exports = Chart;
