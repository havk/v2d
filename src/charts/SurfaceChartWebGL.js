'use strict';

var d3 = require('d3');
var _ = require('lodash');
var THREE = require('three');

var Chart = require('./Chart');
var TrackballControls = require('../../lib/TrackballControls');

function SurfaceChartWebGL(cfg) {
    Chart.call(this, cfg);

    this.cfg.parseTime = this.cfg.parseTime || d3.time.format('%Y%m%d %X').parse;
    this.cfg.width = this.cfg.width || 500;
    this.cfg.height = this.cfg.height || 500;
    this.cfg.viewAngle = this.cfg.viewAngle || 45;
    this.cfg.near = this.cfg.near || 1;
    this.cfg.far = this.cfg.far || 5000;
    this.cfg.xMax = this.cfg.xMax || 500;
    this.cfg.yMax = this.cfg.yMax || 300;
    this.cfg.zMax = this.cfg.zMax || 100;
    this.cfg.xMin = this.cfg.xMin || -500;
    this.cfg.yMin = this.cfg.yMin || -300;
    this.cfg.zMin = this.cfg.zMin || -100;
    //this.cfg.xMax = this.cfg.xMax || 10;
    //this.cfg.yMax = this.cfg.yMax || 10;
    //this.cfg.zMax = this.cfg.zMax || 10;
}

SurfaceChartWebGL.prototype = Object.create(Chart.prototype);
SurfaceChartWebGL.constructor = SurfaceChartWebGL;

SurfaceChartWebGL.prototype.createCamera = function() {
    var aspect = this.cfg.width / this.cfg.height;
    var camera = new THREE.PerspectiveCamera(this.cfg.viewAngle, aspect, this.cfg.near, this.cfg.far);

    return camera;
};

SurfaceChartWebGL.prototype.createRenderer = function() {
    var renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(this.cfg.width, this.cfg.height);

    return renderer;
};

SurfaceChartWebGL.prototype.addLight = function(scene) {
    var positions = [[1, 1, 1 ], [-1, -1, 1], [-1, 1, 1], [1, -1, 1]];

    for (var i = 0; i < 4; i++) {
        var light = new THREE.DirectionalLight(0xdddddd);
        light.position.set(positions[i][0], positions[i][1], 0.4 * positions[i][2]);
        scene.add(light);
    }
};

SurfaceChartWebGL.prototype.init = function(parentID) {
    var scene = new THREE.Scene();
    var camera = this.createCamera();
    var renderer = this.createRenderer();

    scene.add(camera);
    camera.position.set(0, -950, 250);
    camera.lookAt(scene.position);

    var container = document.getElementById(parentID);
    container.appendChild(renderer.domElement);

    this.addLight(scene);

    var controls = new TrackballControls(camera);

    renderer.setClearColor(0xffffff, 1);

    return {
        scene: scene,
        camera: camera,
        renderer: renderer,
        controls: controls
    };
};

SurfaceChartWebGL.prototype.render = function(components) {
    components.renderer.render(components.scene, components.camera);
};

SurfaceChartWebGL.prototype.animate = function(components) {
    var that = this;
    requestAnimationFrame(function() {
        that.animate(components);
    });
    this.render(components);
    components.controls.update();
};

SurfaceChartWebGL.prototype.initGrid = function(components) {
    var scene = components.scene;
    var BIGIN = -10, END = 10, WIDTH = END - BIGIN;
    var planeGeometry = new THREE.PlaneGeometry(WIDTH, WIDTH);
    var planeMaterial = new THREE.MeshLambertMaterial({color: 0xf0f0f0, shading: THREE.FlatShading, overdraw: 0.5, side: THREE.DoubleSide});
    var plane = new THREE.Mesh(planeGeometry, planeMaterial);
    scene.add(plane);

    var geometry = new THREE.Geometry();

    for(var i = BIGIN; i <= END; i += 2){
        geometry.vertices.push(new THREE.Vector3(BIGIN, i, 0));
        geometry.vertices.push(new THREE.Vector3(END, i, 0));
        geometry.vertices.push(new THREE.Vector3(i, BIGIN, 0));
        geometry.vertices.push(new THREE.Vector3(i, END, 0));
    }

    var material = new THREE.LineBasicMaterial( { color: 0x999999, opacity: 0.2 } );

    var line = new THREE.Line(geometry, material);
    line.type = THREE.LinePieces;
    scene.add(line);
};

SurfaceChartWebGL.prototype.createScaleX = function(data) {
    var xData = data.map(function(d) { return d.x; });
    var scaleX = d3.scale.ordinal()
        .domain(_.sortBy(_.uniq(xData)))
        .rangeRoundBands([this.cfg.xMin, this.cfg.xMax], 0);

    return scaleX;
};

SurfaceChartWebGL.prototype.createScaleY = function(data) {
    var yData = data.map(function(d) { return d.y; });
    var scaleY = d3.scale.ordinal()
        .domain(_.sortBy(_.uniq(yData)))
        .rangeRoundBands([this.cfg.yMin, this.cfg.yMax]);

    return scaleY;
};

SurfaceChartWebGL.prototype.createScaleZ = function(data) {
    var zData = data.map(function(d) { return d.z; });
    var max = d3.max(zData);
    var min = d3.min(zData);

    var scaleZ = d3.scale.linear()
        .domain([min, max])
        .range([this.cfg.zMin, this.cfg.zMax]);

    return scaleZ;
};

SurfaceChartWebGL.prototype.createScaleColor = function(data) {
    var zData = data.map(function(d) { return d.z; });
    var max = d3.max(zData);
    var min = d3.min(zData);

    var scaleC = d3.scale.linear()
        .domain([min, parseFloat((max - min) / 2.0) + parseFloat(min), max])
        .range(['blue', 'yellow', 'red']);

    return scaleC;
};

SurfaceChartWebGL.prototype.drawData = function(scene, scales, data) {
    var geometry = new THREE.Geometry();
    var colors = [];

    data.d.forEach(function(row) {
        for (var i = 0, l = row.z.length; i < l; i++) {
            var color = scales.c(row.z[i]);
            var z = scales.z(row.z[i]);
            var x = scales.x(row.x);
            var y = scales.y(data.y[i]);

            geometry.vertices.push(new THREE.Vector3(x, y, z));
            colors.push(new THREE.Color(color));
        }
    });

    var xLen = data.d.length, yLen = data.y.length;

    var offset = function(x, y){
        return x * yLen + y;
    };

    for (var x = 0; x < xLen - 1; x++) {
        for (var y = 0; y < yLen - 1; y++) {
            var vec0 = new THREE.Vector3(), vec1 = new THREE.Vector3(), nVec = new THREE.Vector3();
            // one of two triangle polygons in one rectangle
            vec0.subVectors(
                geometry.vertices[offset(x, y)],
                geometry.vertices[offset(x + 1, y)]
            );
            vec1.subVectors(
                geometry.vertices[offset(x, y)],
                geometry.vertices[offset(x, y + 1)]
            );
            nVec.crossVectors(vec0, vec1).normalize();
            geometry.faces.push(new THREE.Face3(
                offset(x, y), offset(x + 1, y), offset(x, y + 1), nVec,
                [colors[offset(x, y)], colors[offset(x + 1, y)], colors[offset(x, y + 1)]]
            ));
            geometry.faces.push(new THREE.Face3(
                offset(x, y), offset(x, y + 1), offset(x + 1, y), nVec.negate(),
                [colors[offset(x, y)], colors[offset(x, y + 1)], colors[offset(x + 1, y)]]
            ));

            // the other one
            vec0.subVectors(
                geometry.vertices[offset(x + 1, y)],
                geometry.vertices[offset(x + 1, y + 1)]
            );
            vec1.subVectors(
                geometry.vertices[offset(x, y + 1)],
                geometry.vertices[offset(x + 1, y + 1)]
            );
            nVec.crossVectors(vec0, vec1).normalize();
            geometry.faces.push(new THREE.Face3(
                offset(x + 1, y), offset(x + 1, y + 1), offset(x, y + 1), nVec,
                [colors[offset(x + 1, y)], colors[offset(x + 1, y + 1)], colors[offset(x, y + 1)]]
            ));
            geometry.faces.push(new THREE.Face3(
                offset(x + 1, y), offset(x, y + 1), offset(x + 1, y + 1), nVec.negate(),
                [colors[offset(x + 1, y)], colors[offset(x, y + 1)], colors[offset(x + 1, y + 1)]]
            ));
        }
    }

    var material = new THREE.MeshLambertMaterial({vertexColors: THREE.VertexColors});
    var mesh = new THREE.Mesh(geometry, material);

    scene.add(mesh);
};

SurfaceChartWebGL.prototype.prepareData = function(data) {
    var xData = _.sortedUniq(_.sortBy(data.map(function(d) { return d.x; })));
    var yData = _.sortedUniq(_.sortBy(data.map(function(d) { return d.y; })));
    var sortedData = _.sortBy(data, ['x', 'y']);
    var rows = [];
    var yDict = {};
    var xDict = {};
    var average = _.sum(data.map(function(d) { return d.z; })) / data.length;

    yData.forEach(function(y, i) {
        yDict[y] = i;
    });
    xData.forEach(function(x, i) {
        xDict[x] = i;
        rows.push({'x': x, 'z': []});
    });

    sortedData.forEach(function(d) {
        rows[xDict[d.x]].z[yDict[d.y]] = d.z;
    });

    for (var i = 0, len = xData.length; i < len; i++) {
        for (var j = 0, len2 = yData.length; j < len2; j++) {
            if (rows[i].z[j] === undefined) {
                rows[i].z[j] = average;
            }
        }
    }
    console.log({'y': yData, 'd': rows});

    return {'y': yData, 'd': rows};
};

SurfaceChartWebGL.prototype.draw = function(parentID, data) {
    var newData = this.prepareData(data);
    var scales = {
        x: this.createScaleX(data),
        y: this.createScaleY(data),
        z: this.createScaleZ(data),
        c: this.createScaleColor(data)
    };

    var components = this.init(parentID);

    //this.initGrid(components);
    this.drawData(components.scene, scales, newData);
    this.animate(components);
};

module.exports = SurfaceChartWebGL;
