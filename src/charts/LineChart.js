'use strict';

var d3 = require('d3');
var Chart = require('./Chart');

function LineChart(cfg) {
    Chart.call(this, cfg);
    this.cfg.parseTime = this.cfg.parseTime || d3.time.format('%Y-%m-%d %X').parse;
}

LineChart.prototype = Object.create(Chart.prototype);
LineChart.constructor = LineChart;

LineChart.prototype.createScaleX = function(data) {
    var that = this;
    var max = d3.max(data, function(d) {
        return d3.max(d.x, function(x) {
            if (that.cfg.timeOnX === true) {
                return that.cfg.parseTime(x);
            }
            return x;
        });
    });
    var min = d3.min(data, function(d) {
        return d3.min(d.x, function(x) {
            if (that.cfg.timeOnX === true) {
                return that.cfg.parseTime(x);
            }
            return x;
        });
    });
    var scaleX = null;

    if (this.cfg.timeOnX === true) {
        //min = this.cfg.parseTime(min);
        //max = this.cfg.parseTime(max);
        scaleX = d3.time.scale()
            .domain([min, max])
            .range([0, this.cfg.w]);
    } else {
        scaleX = d3.scale.linear()
            .domain([min, max])
            .range([0, this.cfg.w]);
    }

    return scaleX;
};

LineChart.prototype.createScaleY = function(data) {
    var max = d3.max(data, function(d) {
        return d3.max(d.y);
    });
    var min = d3.min(data, function(d) {
        return d3.min(d.y);
    });
    var scaleY = d3.scale.linear()
        .domain([max, min])
        .range([0, this.cfg.h]);

    return scaleY;
};

LineChart.prototype.drawXAxis = function(svg, scaleX, data) {
    var xAxis = svg.select('#xAxis');

    if (xAxis.empty()) {
        xAxis = svg.append('g')
            .attr('id', 'xAxis')
            .attr('class', 'axis')
            .attr('transform', 'translate(0,' + this.cfg.h + ')');
    }

    var axis = d3.svg.axis()
        .scale(scaleX)
        .orient('bottom');

    if (data[0].x.length < this.cfg.width / 50) {
        axis.ticks(data[0].x.length);
    } else {
        axis.ticks(this.cfg.width / 50);
    }

    if (this.cfg.timeOnX === true) {
        axis.tickFormat(this.cfg.timeFormat);
        xAxis.call(axis)
            .selectAll('text')
                .style('text-anchor', 'end')
                .attr('dx', '-.8em')
                .attr('dy', '.15em')
                .attr('transform', function() {
                    return 'rotate(-65)';
                });
    } else {
        xAxis.call(axis);
    }
};

LineChart.prototype.drawYAxis = function(svg, scaleY, data) {
    var yAxis = svg.select('#yAxis');

    if (yAxis.empty()) {
        yAxis = svg.append('g')
            .attr('id', 'yAxis')
            .attr('class', 'axis');
    }

    var axis = d3.svg.axis()
        .scale(scaleY)
        .orient('left');

    if (data[0].y.length < this.cfg.height / 50) {
        axis.ticks(data[0].y.length);
    } else {
        axis.ticks(this.cfg.height / 50);
    }

    yAxis.transition().call(axis);
};

LineChart.prototype.drawData = function(svg, scaleX, scaleY, data) {
    //this.drawPoints(svg, scaleX, scaleY, data);
    this.drawLines(svg, scaleX, scaleY, data);
};

LineChart.prototype.drawPoints = function(svg, scaleX, scaleY, data) {
    this.svg.selectAll('circle')
        .data(data.x)
        .enter()
        .append('circle')
            .attr('cx', function (d, i) { return scaleX(data.x[i]); })
            .attr('cy', function (d, i) { return scaleY(data.y[i]); })
            .attr('r', 2)
            .style('fill', 'black');
};

LineChart.prototype.drawLines = function(svg, scaleX, scaleY, data) {
    var lineFunc = null;
    var that = this;
    var colors = d3.scale.category10().domain(d3.range(10));

    var lines = svg.selectAll('#line').data(data);

    lines.enter()
        .append('svg:path')
            .attr({
                'id': 'line',
                'stroke': function(d, i) {
                    return colors(i);
                },
                'stroke-width': 2,
                'fill': 'none'
            });

    lines.transition().attr({
        'd': function(data) {
            if (that.cfg.timeOnX === true) {
                lineFunc = d3.svg.line()
                    .x(function(d, i) { return scaleX(that.cfg.parseTime(data.x[i])); })
                    .y(function(d, i) { return scaleY(data.y[i]); })
                    .interpolate('linear');
            } else {
                lineFunc = d3.svg.line()
                    .x(function(d, i) { return scaleX(data.x[i]); })
                    .y(function(d, i) { return scaleY(data.y[i]); })
                    .interpolate('linear');
            }
            return lineFunc(data.x);
        },
        'stroke-width': function(d, i) {
            return 2 * (data.length - i);
        }
    });

    lines.exit().remove();
};

LineChart.prototype.draw = function(parentID, data) {
    if (data.length === 0) {
        return;
    }

    this.svg = this.svg || this.createSVG(parentID);

    var scaleX = this.createScaleX(data);
    var scaleY = this.createScaleY(data);

    this.drawXAxis(this.svg, scaleX, data);
    this.drawYAxis(this.svg, scaleY, data);
    this.drawData(this.svg, scaleX, scaleY, data);

    this.beautify(this.svg);
};

LineChart.prototype.clear = function() {
    this.svg.selectAll('#line').remove();
};

module.exports = LineChart;
