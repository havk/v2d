'use strict';

var d3 = require('d3');
var _ = require('lodash');
var Chart = require('./Chart');

function Heatmap(cfg) {
    Chart.call(this, cfg);
}

Heatmap.prototype = Object.create(Chart.prototype);
Heatmap.constructor = Heatmap;

Heatmap.prototype.createScaleX = function(data) {
    var xData = data.map(function(d) { return d.x; });
    var scaleX = d3.scale.ordinal()
        .domain(_.sortBy(_.uniq(xData), function(x) {
            return parseInt(x.replace(/:/g, ''));
        }))
        .rangeRoundBands([0, this.cfg.w], 0);

    return scaleX;
};

Heatmap.prototype.createScaleY = function(data) {
    var yData = data.map(function(d) { return d.y; });
    var scaleY = d3.scale.ordinal()
        .domain(_.sortBy(_.uniq(yData)))
        .rangeRoundBands([this.cfg.h, 0]);

    return scaleY;
};

Heatmap.prototype.createScaleZ = function(data) {
    var zData = data.map(function(d) { return d.z; });
    var max = d3.max(zData);
    var min = d3.min(zData);

    var scaleZ = d3.scale.linear()
        .domain([min, parseFloat((max - min) / 2.0) + parseFloat(min), max])
        .range(['blue', 'yellow', 'red']);

    return scaleZ;
};

Heatmap.prototype.drawXAxis = function(svg, scaleX, data) {
    var xData = _.uniq(data.map(function(d) { return d.x; }));
    var filter = 1;
    if (xData.length > this.cfg.width / 50) {
        filter = Math.floor(xData.length / (this.cfg.width / 50));
    }

    svg.append('g')
        .attr('class', 'axis')
        .call(d3.svg.axis()
            .scale(scaleX)
            .tickValues(scaleX.domain().filter(function(d, i) {
                if (i % filter === 0) {
                    return 1;
                }
                return 0;
            }))
            .orient('bottom'))
        .attr('transform', 'translate(0,' + this.cfg.h + ')')
        .selectAll('text')
            .style('text-anchor', 'end')
            .attr('dx', '-.8em')
            .attr('dy', '.15em')
            .attr('transform', function() {
                return 'rotate(-65)';
            });
};

Heatmap.prototype.drawYAxis = function(svg, scaleY, data) {
    var yData = _.uniq(data.map(function(d) { return d.y; }));
    var filter = 1;
    if (yData.length > this.cfg.height / 50) {
        filter = Math.floor(yData.length / (this.cfg.height / 50));
    }
    svg.append('g')
        .attr('class', 'axis')
        .call(d3.svg.axis()
            .scale(scaleY)
            .tickValues(scaleY.domain().filter(function(d, i) {
                if (i % filter === 0) {
                    return 1;
                }
                return 0;
            }))
            .orient('left'));
};

Heatmap.prototype.drawZAxis = function(svg, scaleZ, data) {
    var color = scaleZ;
    var width = 10;
    var height = this.cfg.h;
    var domain = scaleZ.domain();
    var min = domain[0];
    var mid = domain[1];
    var max = domain[2];

    var y = d3.scale.linear()
        .domain([min, max])
        .range([height, 0]);

    var g = svg.append('g');

    // SVG defs
    var defs = g
        .datum({min: min, mid: mid})
        .append('svg:defs');

    // Gradient defs
    var gradient1 = defs.append('svg:linearGradient')
        .attr('id', 'gradient1')
        .attr('x1', '0%')
        .attr('y1', '0%')
        .attr('x2', '0%')
        .attr('y2', '100%');
    var gradient2 = defs.append('svg:linearGradient')
        .attr('id', 'gradient2')
        .attr('x1', '0%')
        .attr('y1', '0%')
        .attr('x2', '0%')
        .attr('y2', '100%');

    // Gradient 1 stop 1
    gradient1.append('svg:stop')
        .datum({max: max})
        .attr('stop-color', function(d) { return color(d.max) })
        .attr('offset', '0%');

    // Gradient 1 stop 2
    gradient1.append('svg:stop')
        .datum({mid: mid})
        .attr('stop-color', function(d) { return color(d.mid) })
        .attr('offset', '100%');

    // Gradient 2 stop 1
    gradient2.append('svg:stop')
        .datum({mid: mid})
        .attr('stop-color', function(d) { return color(d.mid) })
        .attr('offset', '0%');

    // Gradient 2 stop 2
    gradient2.append('svg:stop')
        .datum({min: min})
        .attr('stop-color', function(d) { return color(d.min) })
        .attr('offset', '100%');

    // Gradient 1 rect
    g
        .datum({min: min, mid: mid })
        .append('svg:rect')
            .attr('id', 'gradient1-bar')
            .attr('fill', 'url(#gradient1)')
            .attr('height', function(d) { return y(d.mid) })
            .attr('width', width);

    // Gradient 2 rect
    g
        .datum({mid: mid, max: max})
        .append('svg:rect')
            .attr('id', 'gradient2-bar')
            .attr('fill', 'url(#gradient2)')
            .attr('transform', function(d) { return 'translate(0,' + y(d.mid) + ')'})
            .attr('height', function(d) { return y(d.max) + y(d.mid) })
            .attr('width', width);

    // Append axis
    var axis = d3.svg.axis()
        .scale(y)
        .tickValues([min, mid, max])
        .orient('right');

    g.append('g').attr('class', 'axis');

    g.selectAll('.axis')
        .attr('transform', 'translate('+(width)+', 0)')
        .call(axis);

    g.attr('transform', 'translate('+(this.cfg.w + 10)+', 0)');

    g.selectAll('path')
        .style('display', 'none');
};

Heatmap.prototype.drawData = function(svg, scales, data) {
    var yData = _.uniq(data.map(function(d) { return d.y; }));
    var xData = _.uniq(data.map(function(d) { return d.x; }));
    var sizeY = this.cfg.h / yData.length;
    var sizeX = this.cfg.w / xData.length;

    svg.selectAll('.rect')
        .data(data)
        .enter().append('rect')
        .attr('x', function(d) { return scales.x(d.x); })
        .attr('y', function(d) { return scales.y(d.y); })
        .attr('dx', function(d) { return d.x; })
        .attr('dy', function(d) { return d.y; })
        .attr('dz', function(d) { return d.z; })
        //.attr('rowIdx', function() { return index; })
        //.attr('colIdx', function(d, i) { return i; })
        .attr('width', sizeX)
        .attr('height', sizeY)
        .attr('class', 'heatmap')
        .style('fill', function(d) {return scales.z(d.z); });
};

Heatmap.prototype.drawPointers = function(pointers) {
    console.log(pointers);
    var svg = this.svg;
    var pointersX = svg.selectAll('.pointersX').data(pointers.x);
    var pointersY = svg.selectAll('.pointersY').data(pointers.y);
    var height = svg.node().parentNode.getAttribute('height') - this.cfg.margin.bottom;
    var width = (svg.node().parentNode.getAttribute('width')
                 - this.cfg.margin.right - this.cfg.margin.left);

    pointersX.enter()
        .append('rect')
        .attr({
            'x': function(d) { return d - 1; }, // -1 because y = 3
            'y': 3,
            'width': 1,
            'height': height,
            'stroke': function(d, i) {
                return pointers.colors(pointers.c[i]);
            },
            'class': 'pointersX'
        })
        .style({
            'fill': 'black',
            'pointer-events': 'none'
        });

    pointersY.enter()
        .append('rect')
        .attr({
            'x': 3,
            'y': function(d) { return d - 1; }, // -1 because x = 3
            'width': width,
            'height': 1,
            'stroke': function(d, i) {
                return pointers.colors(pointers.c[i]);
            },
            'class': 'pointersY'
        })
        .style({
            'fill': 'black',
            'pointer-events': 'none'
        });

    pointersX.attr({
        'x': function(d) { return d - 1; }, // -1 because y = 3
        'stroke': function(d, i) {
            return pointers.colors(pointers.c[i]);
        }
    });
    pointersY.attr({
        'y': function(d) { return d - 1; }, // -1 because x = 3
        'stroke': function(d, i) {
            return pointers.colors(pointers.c[i]);
        }
    });

    pointersX.exit().remove();
    pointersY.exit().remove();
};

Heatmap.prototype.createTooltip = function(parentID) {
    var tooltip = d3.select('#' + parentID)
        .append('div')
        .attr('id', 'tooltip')
        .style({
            position: 'absolute',
            display: 'none',
            padding: '5px',
            border: 'solid',
            'text-align': 'center',
            'border-width': '1px',
            'background-color': 'lightyellow',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            'border-radius': '10px'
        });

    tooltip.append('div')
        .attr('id', 'x');

    tooltip.append('div')
        .attr('id', 'y');

    tooltip.append('div')
        .attr('id', 'z');
};

Heatmap.prototype.draw = function(parentID, data) {
    this.svg = this.svg || this.createSVG(parentID);
    this.createTooltip(parentID);

    var scales = {
        x: this.createScaleX(data),
        y: this.createScaleY(data),
        z: this.createScaleZ(data)
    };

    this.drawXAxis(this.svg, scales.x, data);
    this.drawYAxis(this.svg, scales.y, data);
    this.drawZAxis(this.svg, scales.z, data);
    this.drawData(this.svg, scales, data);

    this.beautify(this.svg);
};

Heatmap.prototype.onMouseOver = function(handler) {
    var svg = this.svg;

    svg.node().addEventListener('mouseover', function(event) {
        var target = event.target || event.srcElement;

        if (target.nodeName === 'rect' && target.classList[0] === 'heatmap') {
            //var x = parseInt(target.getAttribute('x')) + target.getAttribute('width') / 2;
            //svg.append('rect')
                //.attr({
                    //'x': x - 1, // -1 because y = 3
                    //'y': 3,
                    //'width': 1,
                    //'height': svg.node().parentNode.getAttribute('height'),
                    //'class': 'pointer'
                //})
                //.style({
                    //'fill': 'black',
                    //'pointer-events': 'none'
                //});

            //var y = parseInt(target.getAttribute('y')) + target.getAttribute('height') / 2;
            //svg.append('rect')
                //.attr({
                    //'x': 3,
                    //'y': y - 1, // -1 because x = 3
                    //'width': svg.node().parentNode.getAttribute('width'),
                    //'height': 1,
                    //'class': 'pointer'
                //})
                //.style({
                    //'fill': 'black',
                    //'pointer-events': 'none'
                //});

            var dx = target.getAttribute('dx');
            var dy = target.getAttribute('dy');
            var dz = target.getAttribute('dz');

            var tooltip = d3.select('#tooltip')
                .style('display', 'block');

            tooltip.select('#x').text(dx);
            tooltip.select('#y').text(dy);
            tooltip.select('#z').text(dz);

            if (handler !== undefined) {
                handler(event);
            }
        }
    });

    svg.node().addEventListener('mousemove', function(event) {
        var target = event.target || event.srcElement;

        if (target.nodeName === 'rect' && target.classList[0] === 'heatmap') {
            d3.select('#tooltip')
                .style('left', Math.max(0, event.pageX + 15) + 'px')
                .style('top', (event.pageY + 25) + 'px');
        }
    });
};

Heatmap.prototype.onMouseOut = function(handler) {
    var svg = this.svg;
    svg.node().addEventListener('mouseout', function(event) {
        svg.selectAll('rect.pointer')
            .remove();

        d3.select('#tooltip')
            .style('display', 'none');

        if (handler !== undefined) {
            handler(event);
        }
    });
};

Heatmap.prototype.onClick = function(handler) {
    var svg = this.svg;

    svg.node().addEventListener('click', function(event) {
        var target = event.target || event.srcElement;

        if (target.nodeName === 'rect') {
            if (handler !== undefined) {
                handler(event);
            }
        }
    });
};


module.exports = Heatmap;
