'use strict';

var cfg = {
    width: 1000,
    height: 600,
    margin: {left: 80, top: 10, right: 10, bottom: 50}
};

var V2D = V2D || {};

var SurfaceChart = new V2D.SurfaceChartWebGL(cfg);

V2D.d3.csv('../data/newdata.csv')
    .row(function(d) { return {x: d.x, y: d.y, z: d.z}; })
    .get(function(error, rows) {SurfaceChart.draw('chart', rows); });
