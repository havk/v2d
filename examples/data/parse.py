import csv

newFile = open('newdata.csv', 'a+')
writer = csv.writer(newFile)
last = -1

with open('dataaa.csv', 'a+') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    newData = []
    for row in reader:
        time = row[1]
        vals = time.split(':')
        if (int(vals[1]) / 20 * 2) != last:
            new = '{}:{}0'.format(vals[0], str(int(vals[1]) / 20 * 2))
            row[1] = new
            newData.append(row)
            last = (int(vals[1]) / 20 * 2)

    writer.writerows(newData)

newFile.close()
