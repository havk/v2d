'use strict';


var cfg = {
    width: 1200,
    height: 500,
    margin: {left: 80, top: 10, right: 10, bottom: 200}
};

var V2D = V2D || {};

var heatmap = new V2D.Heatmap(cfg);

V2D.d3.csv('../data/data.csv')
    .row(function(d) { return {x: d.x, y: d.y, z: d.z}; })
    .get(function(error, rows) { console.log(rows); heatmap.draw('chart', rows); });
