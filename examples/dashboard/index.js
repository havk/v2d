'use strict';

var V2D = V2D || {};

var dashboard = new V2D.Dashboard();

V2D.d3.csv('../data/newdata.csv')
    .row(function(d) { return {x: d.x, y: d.y, z: d.z}; })
    .get(function(error, rows) {dashboard.draw('#dashboard', rows); });
